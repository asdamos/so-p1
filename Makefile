CC=gcc
CFLAGS=-std=c99 -pthread
EXECUTABLE=program

program:
	$(CC) $(CFLAGS) main.c -o Program

run:
	./Program

clean:
	rm Program
