//Systemy Operacyjne - Pracownia 1
//Adam Sawicki
//Indeks: 270814

#include <stdio.h>
#include <pthread.h>

void *thread_function(void *arg)				//function that will be run by new thread
{
    printf("Created new thread\n");
    return NULL;
}

int main()
{
    pthread_t thread1;

    if(pthread_create(&thread1, NULL, thread_function, NULL))	//creating thread
    {
        printf("Error creating thread\n");
        return 1;
    }

    if(pthread_join(thread1,NULL))				//joining thread 
    {
        printf("Error joining thread\n");
        return 2;
    }
    return 0;
}

